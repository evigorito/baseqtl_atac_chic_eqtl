// negative and beta binomial for ASE eQTL with imputed genotypes. Allows for any mixture of gaussians for bj prior (eQTL effect). Total counts and SNP mapped counts are not necessarily the same. Allows for ref panel correction.
 
data {
  int<lower=0> N; // number of individuals
  int<lower=0> G; // number of total genotypes for all individuals
  int<lower=0> K; // number of covariates plus intercept
  int<lower=0> k; // number of Gaussians for eQTL effect prior
  int Y[N]; // total counts
  int m[N]; // mapped total allelic specficic counts
  int n[N]; // alt allele specific counts
  int sNB[N]; //  number of possible genotypes NB for each individual
  vector[G] g; // each genotype (0,1,2)
  vector[G] pg; // prob for each genotype
  real ai0; //allelic imbalance estimate for each sample in log scale
  real<lower=0> sdai0; // standard deviation for allelic imbalance estimate for each sample
  matrix[N,1+K] cov;
  vector[k] aveP; // mean for prior Gaussians for eQTL effect prior
  vector[k] sdP; // sd for prior Gaussians for eQTL effect prior
  vector[k] mixP; // log of mixing proportions for eQTL effect prior

  }

parameters {
  vector[K] betas; // regression param
  real bj; // log fold change ASE
  real<lower=0> phis; //overdipersion param for neg binom
  real<lower=0> theta; //the overdispersion parameter for beta binomial
  vector[N] rai0; //random intercept ref panel bias
}

model {
  // include transformed parameters of no interest
  vector[N] lmu1;//the linear predictor
  real lmu; 
  real p; // ASE proportion 
  real ebj;
  real ebjd; // reduce computation
  int pos; // to loop over haplotypes for each individual
  vector[G] ltmp; //  log NB likelihood
  real ase; // log BB
  vector[k] lps; // help for mixed gaussians


  // Priors
  theta ~ gamma(1,0.1); //  mean 10 
  phis ~ gamma(1,0.1);  // mean 10
  for(i in 1:K){
    betas[i] ~ cauchy(0,2.5);//prior for the slopes following Gelman 2008   
      }
  // mixture of gaussians for bj:
  for(i in 1:k){
    lps[i] = normal_lpdf(bj | aveP[i], sdP[i]) + mixP[i];
  }
  target += log_sum_exp(lps);

  // reference panel bias
  for(i in 1:N){
    rai0[i] ~ normal(ai0, sdai0);
  }



  // Likelihood
  pos=1;
  ebj=exp(bj); // avoid repeating same calculation
  lmu1 = cov[,2:cols(cov)]*betas;
  
  for(i in 1:N){ // neg binomial
     for (r in pos:(pos+sNB[i]-1)){

       lmu = lmu1[i];
	 
       lmu = g[r]==1 ? lmu1[i] + log1p(ebj)-log(2) : lmu1[i];

       lmu = g[r]==2 ? lmu + bj : lmu;

       ltmp[r] = neg_binomial_2_lpmf(Y[i] | exp(lmu), phis) + log(pg[r]);
      
       if (m[i] > 0 && g[r] == 1 ) { // ASE counts, beta binomial, add ASE contribution to NB contribution

	 p = inv_logit(rai0[i] + bj);

       	 ase = beta_binomial_lpmf(n[i] | m[i], p*theta , (1-p)*theta);

       	 /* ltmp[r] =  log_sum_exp(ltmp[r] , ase); */
	 ltmp[r] = ase + ltmp[r];
       }
       
     }
     
     target += log_sum_exp(ltmp[pos:(pos+sNB[i]-1)]);
     
     pos=pos+sNB[i];
  }
}




